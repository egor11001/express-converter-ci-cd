import express from 'express';
import { healthController } from './health.controller';
import { routes as filesRoutes } from './files/route';
import { routes as authRoutes } from './auth/route';
import { routes as payRoutes } from './pay/route';

const routes = express.Router();

routes.get('/health', healthController);
routes.use('/files', filesRoutes);
routes.use('/auth', authRoutes);
routes.use('/pay', payRoutes);

export { routes };
