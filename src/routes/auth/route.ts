import { GoogleCallback } from './controllers/google.callback';
import express from 'express';
import passport from 'passport';

import { PrivateController } from './controllers/private.controller';
import { RefreshController } from './controllers/refresh.controller';
import { PassportValidation } from './passport.validation';

const routes = express.Router();

routes.use('/private', PassportValidation, PrivateController);
routes.use('/refresh', RefreshController, passport.authenticate('custom', { failureRedirect: '/', successRedirect: '/' }));

routes.get('/google', passport.authenticate('google', { scope: ['profile', 'email'] }));
routes.get(
  '/google/callback',
  passport.authenticate('google', {
    failureRedirect: '/',
    failureMessage: true,
  }),
  GoogleCallback
);

export { routes };
