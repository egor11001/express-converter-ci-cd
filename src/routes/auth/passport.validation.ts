import { Request, Response, NextFunction } from 'express';
import * as constants from './constants';
import { AuthRequest } from '../../types/users';

export const PassportValidation = (req: Request, res: Response, next: NextFunction) => {
  const authReq = req as AuthRequest;

  if (authReq.user) {
    return next();
  }
  return res.status(401).json(constants.ERR_ACCESS);
};
