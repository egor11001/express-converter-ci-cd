import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import fs from 'fs';

import { AuthRequest } from '../../../types/users';
import { db_paths } from '../../../db/db.paths';

export const GoogleCallback = async (req: Request, res: Response) => {
  try {
    const authReq = req as AuthRequest;
    const { id } = authReq.user!;

    if (!id) {
      throw Error;
    }

    const token = jwt.sign(id, process.env.JWT_SECRET_KEY!);
    const db = await JSON.parse(fs.readFileSync(db_paths.TOKENS_PATH!, 'utf8'));

    db[token] = id;
    fs.writeFileSync(db_paths.TOKENS_PATH!, JSON.stringify(db));

    return res.json({ data: { token } });
  } catch {
    return res.redirect('/');
  }
};
