import { AuthRequest } from './../../../types/users.d';
import { Request, Response } from 'express';

export const PrivateController = (req: Request, res: Response) => {
  const authReq = req as AuthRequest;

  return res.json({
    status: 'ok',
    message: 'Congratulations ' + authReq.user?.username + '. You have a token.',
  });
};
