import { PremiumValidate } from './controllers/premium.validate';
import express from 'express';

import { PremiumController } from './controllers/premium.controller';
import { PassportValidation } from '../auth/passport.validation';

const routes = express.Router();

routes.post('/premium', PassportValidation, PremiumValidate, PremiumController);

export { routes };
