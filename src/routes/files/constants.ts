/* eslint-disable prettier/prettier */
export const MAX_FILE_SIZE = 52428800;
export const FILE_TYPE = 'image';
export const FILE_DIR = 'public';
export const UNAUTH_FREE_UPLOADS = 20;
export const AUTH_FREE_UPLOADS = 20;

export const ERR_ACCESS = 'No access';
export const ERR_FILE_FOUND = 'File not found';
export const ERR_FILE_UPLOAD = 'No files uploaded';
export const ERR_FILE_OVERRIDE = 'Upload file again';
export const ERR_UPLOAD_LIMIT = 'Upload limit exceeded';

export const formats = ['jpeg', 'png', 'webp', 'gif', 'avif', 'tiff', 'svg'];
export const mimetypes = ['image/jpeg', 'image/png', 'image/webp', 'image/gif', 'image/avif', 'image/tiff', 'image/svg'];
