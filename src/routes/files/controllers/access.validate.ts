import { Request, Response, NextFunction } from 'express';
import Joi from 'joi';
import dayjs from 'dayjs';
import fs from 'fs';

import * as constants from '../constants';
import { AuthRequest, UnauthDb } from '../../../types/users';
import { db_paths } from '../../../db/db.paths';

const unauthUserSchema = Joi.object({
  format: Joi.string().valid('webp').required(),
  files: Joi.array()
    .items(
      Joi.object({
        size: Joi.number().max(constants.MAX_FILE_SIZE).required(),
        mimetype: Joi.string().valid('image/jpeg').required(),
        name: Joi.string(),
        data: Joi.binary().required(),
        encoding: Joi.string(),
        md5: Joi.string().required(),
        tempFilePath: Joi.any(),
        truncated: Joi.any(),
        mv: Joi.any(),
      })
    )
    .required(),
});

export const CheckAccess = async (req: Request, res: Response, next: NextFunction) => {
  const authReq = req as AuthRequest;
  const user = authReq.user;
  const { authorization } = req.headers;
  const { format } = req.body;
  let { files } = req.files!;

  if (files && !Array.isArray(files)) {
    files = [files];
  }

  try {
    /* if (!files) {
      throw Error;
    } */

    if (!user) {
      const { error } = unauthUserSchema.validate({ format, files });

      if (error) {
        throw new Error(error.message);
      }

      if (files.length > constants.UNAUTH_FREE_UPLOADS) {
        throw new Error(`Available downloads: ${constants.UNAUTH_FREE_UPLOADS}`);
      }

      if (authorization) {
        const db: UnauthDb = await JSON.parse(fs.readFileSync(db_paths.UNAUTH_PATH!, 'utf8'));

        for (let i = 0; i < db.unauth.length; i++) {
          if (db.unauth[i].id === authorization) {
            if (db.unauth[i].uploaded.length > 1) {
              while (
                dayjs(dayjs(db.unauth[i].uploaded[0].timestamp).format('MM/DD/YYYY')).diff(
                  dayjs(dayjs(db.unauth[i].uploaded[db.unauth[i].uploaded.length - 1].timestamp).format('MM/DD/YYYY')),
                  'month'
                ) > 0
              ) {
                db.unauth[i].uploaded = db.unauth[i].uploaded.slice(0, 1);
              }
            }

            let availableUploads = constants.AUTH_FREE_UPLOADS;
            db.unauth[i].uploaded.forEach(item => {
              availableUploads -= item.count;
            });

            if (availableUploads < 1) {
              throw new Error(constants.ERR_UPLOAD_LIMIT);
            }

            if (availableUploads < files.length) {
              throw new Error(`Available downloads: ${availableUploads}`);
            }

            break;
          }
        }

        fs.writeFileSync(db_paths.FILES_PATH!, JSON.stringify(db));

        return next();
      }

      return next();
    }

    if (user.uploaded.length > 1) {
      while (
        dayjs(dayjs(user.uploaded[0].timestamp).format('MM/DD/YYYY')).diff(
          dayjs(dayjs(user.uploaded[user.uploaded.length - 1].timestamp).format('MM/DD/YYYY')),
          'day'
        ) > 0
      ) {
        user.uploaded = user.uploaded.slice(0, 1);
      }
    }

    if (user.premium > 0) {
      let availableUploads = user.premium + constants.AUTH_FREE_UPLOADS;
      user.uploaded.forEach(item => {
        availableUploads -= item.count;
      });

      if (availableUploads < 1) {
        throw new Error(constants.ERR_UPLOAD_LIMIT);
      }

      if (availableUploads < files.length) {
        throw new Error(`Available downloads: ${availableUploads}`);
      }

      return next();
    }

    if (user.premium === 0) {
      let availableUploads = constants.AUTH_FREE_UPLOADS;
      user.uploaded.forEach(item => {
        availableUploads -= item.count;
      });

      if (availableUploads < 1) {
        throw new Error(constants.ERR_UPLOAD_LIMIT);
      }

      if (availableUploads < files.length) {
        throw new Error(`Available downloads: ${availableUploads}`);
      }

      return next();
    }
  } catch (err: any) {
    return res.json({ data: {}, err: err.message });
  }
};
