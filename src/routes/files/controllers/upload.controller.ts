import { Request, Response } from 'express';
import { nanoid } from 'nanoid/async';
import dayjs from 'dayjs';
import path from 'path';
import fs from 'fs';

import { queueAdd } from '../file-queue';
import { premQueueAdd } from '../prem-queue';
import { ERR_FILE_UPLOAD, FILE_DIR } from '../constants';
import { db_paths } from '../../../db/db.paths';

import { FilesDb, FileDb } from '../../../types/files';
import { AuthRequest, UnauthDb, UsersDb } from '../../../types/users';

export const UploadController = async (req: Request, res: Response) => {
  const authReq = req as AuthRequest;
  // eslint-disable-next-line prefer-const
  let { format, quality } = req.body;
  const { authorization } = req.headers;
  const user = authReq.user;
  let files = req.files?.files;

  quality = Number(quality);

  try {
    if (!files) {
      throw new Error(ERR_FILE_UPLOAD);
    }

    if (files && !Array.isArray(files)) {
      files = [files];
    }

    const db: FilesDb = await JSON.parse(fs.readFileSync(db_paths.FILES_PATH!, 'utf8'));
    let filesToDb: FileDb[] = [];
    let repeatedFiles: string[] = [];

    let token: string;
    if (!user) {
      token = authorization || (await nanoid());
    } else {
      token = user.id;
    }

    for (const file of files) {
      if (user && user.premium > 0) {
        premQueueAdd({ file, quality, token, format });
        continue;
      }

      queueAdd({ file, quality, token, format });
    }

    //update auth user info
    if (user) {
      user.uploaded = [
        ...user.uploaded,
        {
          count: files.length,
          timestamp: dayjs().format('MM/DD/YYYY'),
        },
      ];

      if (user.premium > 0) {
        user.premium -= files.length;
      }

      const usersDb: UsersDb = await JSON.parse(fs.readFileSync(db_paths.USERS_PATH!, 'utf8'));
      for (let i = 0; i < usersDb.users.length; i++) {
        if (usersDb.users[i].id === user.id) {
          usersDb.users[i] = user;
          break;
        }
      }
      fs.writeFileSync(db_paths.USERS_PATH!, JSON.stringify(usersDb));
    }

    //update unauth user info
    if (!user) {
      const unauthDb: UnauthDb = await JSON.parse(fs.readFileSync(db_paths.UNAUTH_PATH!, 'utf8'));
      let isFinded = false;

      for (let i = 0; i < unauthDb.unauth.length; i++) {
        if (unauthDb.unauth[i].id === token) {
          unauthDb.unauth[i].uploaded = [
            ...unauthDb.unauth[i].uploaded,
            {
              count: files.length,
              timestamp: dayjs().format('MM/DD/YYYY'),
            },
          ];
          isFinded = true;
          break;
        }
      }

      if (!isFinded) {
        unauthDb.unauth = [
          ...unauthDb.unauth,
          {
            id: token,
            uploaded: [
              {
                count: files.length,
                timestamp: dayjs().format('MM/DD/YYYY'),
              },
            ],
          },
        ];
      }

      fs.writeFileSync(db_paths.UNAUTH_PATH!, JSON.stringify(unauthDb));
    }

    for (const file of files) {
      try {
        // eslint-disable-next-line node/no-unsupported-features/node-builtins
        await fs.promises.access(path.resolve(FILE_DIR, `${file.md5}.${format}`));
        repeatedFiles = [...repeatedFiles, file.md5];
      } catch {
        continue;
      }
    }

    if (repeatedFiles.length > 0) {
      db.files = db.files.filter(item => {
        return repeatedFiles.find(id => item.id === id) === undefined;
      });
    }

    for (const file of files) {
      filesToDb = [
        ...filesToDb,
        {
          ready: false,
          status: 0,
          user_id: token,
          id: file.md5,
          type: format,
        },
      ];
    }

    //add files info to db
    db.files = db.files.concat(filesToDb);
    fs.writeFileSync(db_paths.FILES_PATH!, JSON.stringify(db));

    return res.json({
      data: {
        token: token,
        files: [filesToDb.map(file => file.id)],
      },
    });
  } catch (err: any) {
    return res.json({ data: {}, err: err.message });
  }
};
