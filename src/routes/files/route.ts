import express from 'express';
import { UploadController } from './controllers/upload.controller';
import { DownloadController } from './controllers/download.controller';
import { UploadValidate } from './controllers/upload.validate';
import { CheckAccess } from './controllers/access.validate';

const routes = express.Router();

routes.post('/upload', UploadValidate, CheckAccess, UploadController);
routes.get('/:id', DownloadController);

export { routes };
