import { fileWorker } from './converter';

let queue: any = [];

export const queueAdd = (file: any) => {
  queue = [...queue, file];
};

const getTasksList = () => {
  let currentTasks: any = [];
  let userIds: string[] = [];
  let currentTasksIndexes: any = [];

  for (let i = 0; i < queue.length; i++) {
    if (currentTasks.length >= 3) {
      break;
    }

    if (!userIds.find(item => item === queue[i].token)) {
      currentTasks = [...currentTasks, queue[i]];
      userIds = [...userIds, queue[i].token];
      currentTasksIndexes = [...currentTasksIndexes, i];
    }
  }

  return { currentTasks, currentTasksIndexes };
};

const doTask = async (tasks: any): Promise<any> => {
  for (let task of tasks) {
    task = await new Promise(resolve => setTimeout(() => resolve(fileWorker(task)), 0));
  }

  return await Promise.all(tasks);
};

export const loop = async () => {
  while (queue.length > 0) {
    const { currentTasks, currentTasksIndexes } = getTasksList();
    await doTask(currentTasks);

    //update queue
    queue = await queue.filter((item: any, index: number) => {
      return currentTasksIndexes.find((taskIndex: number) => taskIndex === index) === undefined;
    });
  }

  setTimeout(() => {
    loop();
  }, 200);
};
