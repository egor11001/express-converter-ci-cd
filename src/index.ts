import express from 'express';
import helmet from 'helmet';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import fileUpload from 'express-fileupload';
import session from 'express-session';
import dayjs from 'dayjs';
import advancedFormat from 'dayjs/plugin/advancedFormat';

import './setup/env.setup';
import { passport } from './routes/auth/passport.setup';

import { routes } from './routes';
import { loop } from './routes/files/file-queue';
import { premLoop } from './routes/files/prem-queue';

const app = express();
const port = process.env.PORT;

app.use(morgan('dev'));
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(fileUpload());
app.use(express.static('/public'));
app.use(session({ secret: process.env.PASSPORT_SECRET_KEY!, resave: true, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());
app.use(routes);

dayjs.extend(advancedFormat);

//start loop worker
premLoop();
loop();

app.listen(port, () => {
  console.log('Server is running');
});
