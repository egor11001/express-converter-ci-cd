import { Request } from 'express';

export interface UsersDb {
  users: UserDb[];
}

export interface UploadedInfo {
  count: number;
  timestamp: string;
}

export interface UserDb {
  id: string;
  username: string;
  password: string;
  email?: string | null;
  source: string;
  uploaded: UploadedInfo[] | [];
  premium: number;
}

export interface UnauthDb {
  unauth: Unauth[];
}

export interface Unauth {
  id: string;
  uploaded: UploadedInfo[] | [];
}

export interface AuthRequest extends Request {
  user?: UserDb;
}
