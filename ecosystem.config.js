module.exports = {
  apps: [
    {
      name: 'app',
      script: './build/src/index.js',
      autorestart: true,
      instances: 'max',
      exec_mode: 'cluster',
      watch: true,
      env: {
        PORT: 3000,
        NODE_ENV: 'development',
      },
      env_production: {
        NODE_ENV: 'production',
      }
    }
  ]
}
